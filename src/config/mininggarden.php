<?php

return [
    'mining_jobmonitor_url' => env('MINING_JOBMONITOR_URL' , null),
    'mining_jobmonitor_enable' => env('MINING_JOBMONITOR_ENABLE' , false),
];