<?php
 
namespace MiningGarden\Monitor;
 
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Support\ServiceProvider;
 
class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any package services.
     */
    public function boot(): void
    {
        // $this->loadRoutesFrom(__DIR__.'/routes/web.php');
        $this->publishes([
            __DIR__.'/config/mininggarden.php' => config_path('mininggarden.php'),
        ]);
    }

    /**
     * Register any application services.
     */
    public function register(): void
    {

    }
}