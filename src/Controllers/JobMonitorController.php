<?php
namespace MiningGarden\Monitor\Controllers;

use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Support\Facades\Log;

class JobMonitorController {
    public static function sendJobMonitor($command_name, $description, $status) {
        $enable_status = config('mininggarden.mining_jobmonitor_enable');
        if ($enable_status) {
            try {
                $data = [
                    'command_name' => $command_name,
                    'job_from' => config('app.name'),
                    'remark' => $description ?? null,
                    'status' => $status
                ];
                $client = new \GuzzleHttp\Client();
                $client->post(config('mininggarden.mining_jobmonitor_url'), [
                    'json' => $data,
                ]);
            } catch (GuzzleException $e) {
                Log::error($e->getMessage());
            }
        }
    }
}